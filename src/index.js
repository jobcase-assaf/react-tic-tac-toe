import * as React from "react";
import ReactDOM from "react-dom";
import "./index.scss";

// const winningStates = (() => {
//     const lines = [
//         [0, 1, 2],
//         [3, 4, 5],
//         [6, 7, 8],
//         [0, 3, 6],
//         [1, 4, 7],
//         [2, 5, 8],
//         [0, 4, 8],
//         [2, 4, 6],
//     ];
//
//     return lines.map(
//         line => line.reduce( (hash, value) => hash + (1 << value), 0 ));
// })

const winningStates = {
    "111000000": true,
    "000111000": true,
    "000000111": true,
    "100010001": true,
    "001010100": true,
    "100100100": true,
    "010010010": true,
    "001001001": true,
};

function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            winner: null,
            playerX: Array(9).fill(0),
            playerO: Array(9).fill(0),
            squares: Array(9).fill(null),
            xIsNext: true,
        };
    }

    handleClick(i) {
        if( this.state.winner ) return;

        const currentPlayerId = this.state.xIsNext ? 'X' : 'O';

        const squares = this.state.squares.slice();
        squares[i] = currentPlayerId;

        const players = {
            "X": this.state.playerX.slice(),
            "O": this.state.playerO.slice()
        }
        const currentPlayer = players[ currentPlayerId ];
        // update individual player state
        currentPlayer[ i ] = 1;

        // check winner
        const isWinner = winningStates[ currentPlayer.join("") ];

        const newState = {
            winner: isWinner ? currentPlayerId : null,
            playerX: players.X,
            playerO: players.O,
            squares,
            xIsNext: !this.state.xIsNext,
        };
        this.setState(newState);
    }

    renderSquare(i) {
        return (
            <Square
                value={this.state.squares[i]}
                onClick={() => this.handleClick(i)}
            />
        );
    }

    render() {
        const winner = this.state.winner;
        let status;
        if( winner ) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div>
                <div className="status">{status}</div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board />
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);

